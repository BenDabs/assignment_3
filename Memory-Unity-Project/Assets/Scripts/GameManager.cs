﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public class GameManager : MonoBehaviour
{
    public static int CurrentLevelNumber;
    public int mistakes;
    public float timer;
    public bool startLevelTimer;
    bool betweenClick = false;
    float timerBetweenClick = 0;
    public int matchCombo;
    int maxCombo;

    [System.Flags]
    public enum Symbols
    {
        Waves = 1 << 0,
        Dot = 1 << 1,
        Square = 1 << 2,
        LargeDiamond = 1 << 3,
        SmallDiamonds = 1 << 4,
        Command = 1 << 5,
        Bomb = 1 << 6,
        Sun = 1 << 7,
        Bones = 1 << 8,
        Drop = 1 << 9,
        Face = 1 << 10,
        Hand = 1 << 11,
        Flag = 1 << 12,
        Disk = 1 << 13,
        Candle = 1 << 14,
        Wheel = 1 << 15
    }

    [System.Serializable]
    public struct LevelDescription
    {
        public int Rows, Columns;
        [EnumFlags]
        public Symbols UsedSymbols;
    }

    public GameObject TilePrefab;
    public float TileSpacing;

    public LevelDescription[] Levels = new LevelDescription[0];
    public Action HideTilesEvent;

    private Tile m_tileOne, m_tileTwo;
    private int m_cardsRemaining;

    private void Start()
    {
        matchCombo = 0;
        startLevelTimer = true;
        timer = 0;
        mistakes = 0;
        LoadLevel(CurrentLevelNumber);
    }

    void Update()
    {
        if (startLevelTimer)
        {
            timer = (timer + Time.deltaTime) % 60;
        }
        if (betweenClick)
        {
            timerBetweenClick = (timerBetweenClick + Time.deltaTime) % 60;
        }
        
    }

    // Loads the level and the symbols into the scene
    private void LoadLevel(int levelNumber)
    {
        LevelDescription level = Levels[levelNumber % Levels.Length];

        List<Symbols> symbols = GetRequiredSymbols(level);

        // Creates the level based on the number of rows, collums and symbols
        for (int rowIndex = 0; rowIndex < level.Rows; ++rowIndex)
        {
            float yPosition = rowIndex * (1 + TileSpacing);
            for (int colIndex = 0; colIndex < level.Columns; ++colIndex)
            {
                float xPosition = colIndex * (1 + TileSpacing);
                GameObject tileObject = Instantiate(TilePrefab, new Vector3(xPosition, yPosition, 0), Quaternion.identity, this.transform);
                int symbolIndex = UnityEngine.Random.Range(0, symbols.Count);
                tileObject.GetComponentInChildren<Renderer>().material.mainTextureOffset = GetOffsetFromSymbol(symbols[symbolIndex]);
                tileObject.GetComponent<Tile>().Initialize(this, symbols[symbolIndex]);
                symbols.RemoveAt(symbolIndex);
            }
        }

        SetupCamera(level);
    }

    //Gets the list of symbols
    private List<Symbols> GetRequiredSymbols(LevelDescription level)
    {
        List<Symbols> symbols = new List<Symbols>();
        int cardTotal = level.Rows * level.Columns;

        //Populates the tiles with pairs of symbols based on the number of tiles
        {
            Array allSymbols = Enum.GetValues(typeof(Symbols));

            m_cardsRemaining = cardTotal;

            if (cardTotal % 2 > 0)
            {
                new ArgumentException("There must be an even number of cards");
            }

            foreach (Symbols symbol in allSymbols)
            {
                if ((level.UsedSymbols & symbol) > 0)
                {
                    symbols.Add(symbol);
                }
            }
        }

        //Checks if there are symbols and if the total of symbols is less than the card total
        {
            if (symbols.Count == 0)
            {
                new ArgumentException("The level has no symbols set");
            }
            if (symbols.Count > cardTotal / 2)
            {
                new ArgumentException("There are too many symbols for the number of cards.");
            }
        }

        //Makes pairs of symbols in scene on cards
        {
            int repeatCount = (cardTotal / 2) - symbols.Count;
            if (repeatCount > 0)
            {
                List<Symbols> symbolsCopy = new List<Symbols>(symbols);
                List<Symbols> duplicateSymbols = new List<Symbols>();
                for (int repeatIndex = 0; repeatIndex < repeatCount; ++repeatIndex)
                {
                    int randomIndex = UnityEngine.Random.Range(0, symbolsCopy.Count);
                    duplicateSymbols.Add(symbolsCopy[randomIndex]);
                    symbolsCopy.RemoveAt(randomIndex);
                    if (symbolsCopy.Count == 0)
                    {
                        symbolsCopy.AddRange(symbols);
                    }
                }
                symbols.AddRange(duplicateSymbols);
            }
        }

        symbols.AddRange(symbols);

        return symbols;
    }

    //
    private Vector2 GetOffsetFromSymbol(Symbols symbol)
    {
        Array symbols = Enum.GetValues(typeof(Symbols));
        for (int symbolIndex = 0; symbolIndex < symbols.Length; ++symbolIndex)
        {
            if ((Symbols)symbols.GetValue(symbolIndex) == symbol)
            {
                return new Vector2(symbolIndex % 4, symbolIndex / 4) / 4f;
            }
        }
        Debug.Log("Failed to find symbol");
        return Vector2.zero;
    }

    //Camera adjust based on number of cards
    private void SetupCamera(LevelDescription level)
    {
        Camera.main.orthographicSize = (level.Rows + (level.Rows + 1) * TileSpacing) / 2;
        Camera.main.transform.position = new Vector3((level.Columns * (1 + TileSpacing)) / 2, (level.Rows * (1 + TileSpacing)) / 2, -10);
    }

    //On click reveals card
    public void TileSelected(Tile tile)
    {
        if (m_tileOne == null)
        {
            betweenClick = true;
            timerBetweenClick = 0;
            m_tileOne = tile;
            m_tileOne.Reveal();
        }
        else if (m_tileTwo == null)
        {
            betweenClick = false;
            AnalyticsEvent.Custom("Time Between Clicks", new Dictionary<string, object>
        {

            {
                "Time Between Clicks", timerBetweenClick
            }

        });
            m_tileTwo = tile;
            m_tileTwo.Reveal();
            if (m_tileOne.Symbol == m_tileTwo.Symbol)
            {
                StartCoroutine(WaitForHide(true, 1f));
            }
            else
            {
                mistakes++;
                StartCoroutine(WaitForHide(false, 1f));
            }
        }
    }

    //When all cards are matched complete level and load new level
    private void LevelComplete()
    {
        startLevelTimer = false;
        AnalyticsEvent.Custom("Time to Complete", new Dictionary<string, object>
        {

            {
                "Time to Complete", timer
            }

        });
        AnalyticsEvent.Custom("Mistake Count", new Dictionary<string, object>
        {

            {
                "Mistake Count", mistakes
            }

        });
        AnalyticsEvent.Custom("Matches in a Row", new Dictionary<string, object>
        {

            {
                "Matches in a Row", matchCombo
            }

        });

        ++CurrentLevelNumber;
        if (CurrentLevelNumber > Levels.Length - 1)
        {
            Debug.Log("GameOver");
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    //Checks if second card matches or not, if not cards trun back over, if yes destroy cards.
    private IEnumerator WaitForHide(bool match, float time)
    {
        float timer = 0;
        while (timer < time)
        {
            timer += Time.deltaTime;
            if (timer >= time)
            {
                break;
            }
            yield return null;
        }
        if (match)
        {
            matchCombo += 1;
            if (maxCombo < matchCombo)
            {
                maxCombo = matchCombo;
            }
           
            Destroy(m_tileOne.gameObject);
            Destroy(m_tileTwo.gameObject);
            m_cardsRemaining -= 2;
        }
        else
        {
            matchCombo = 0;
            m_tileOne.Hide();
            m_tileTwo.Hide();
        }
        
        m_tileOne = null;
        m_tileTwo = null;

        if (m_cardsRemaining == 0)
        {
            LevelComplete();
        }
    }

    private void OnApplicationQuit()
    {
        AnalyticsEvent.Custom("Levels Completed", new Dictionary<string, object>
        {

            {
                "Levels Completed", CurrentLevelNumber
            }

        });

    }
}
